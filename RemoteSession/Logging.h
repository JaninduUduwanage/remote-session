#ifndef _LOGGING_H_
#define _LOGGING_H_
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

class Logging {
private:
	bool IsInitialized;
public:
	Logging();
	~Logging();
	void InitLogging();
};
#endif