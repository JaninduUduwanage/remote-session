#include "stdafx.h"
#include "InputInject.h"

using namespace std;

InputInject* InputInject::InputInjectInstance = NULL;

//Default constructor
InputInject::InputInject() {
}

//Default distructor
InputInject::~InputInject() {
}

//Returns a singleton object of InputInject
InputInject* InputInject::GetInstance() {
	if (!InputInjectInstance) {
		InputInjectInstance = new InputInject();
		return InputInjectInstance;
	}
	return InputInjectInstance;
}

//Inject single mouse event to the input stream
UINT InputInject::InjectMouseInput(LONG X, LONG Y, DWORD MouseData, DWORD MouseFlags){
	INPUT MouseInput;
	MouseInput.type = 0;
	MouseInput.mi.dx = X;
	MouseInput.mi.dy = Y;
	MouseInput.mi.mouseData = MouseData;
	MouseInput.mi.dwFlags = MouseFlags;
	MouseInput.mi.time = 0;
	MouseInput.mi.dwExtraInfo = NULL;
	INPUT InputInjectArray[1] = { MouseInput };
	return SendInput(1, InputInjectArray, sizeof(INPUT));
}

//Inject single keyboard event to the input stream
UINT InputInject::InjectKeyboardInput(WORD KeyCode, DWORD KeyboardFlag){
	INPUT KeyboardInput;
	KeyboardInput.type = 1;
	KeyboardInput.ki.wVk = KeyCode;
	KeyboardInput.ki.wScan = NULL;
	KeyboardInput.ki.dwFlags = KeyboardFlag;
	KeyboardInput.ki.time = 0;
	KeyboardInput.ki.dwExtraInfo = NULL;
	INPUT InputInjectArray[1] = { KeyboardInput };
	return SendInput(1, InputInjectArray, sizeof(INPUT));
}
