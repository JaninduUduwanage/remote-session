//#include "WebConnection.h"
/*
#include <iostream>
#include <vector>
#include "WinDesktopDup.h"
#include "RenderTextureClass.h"

using namespace std;
using namespace web;
using namespace web::websockets::client;
using namespace web::json;
using namespace utility::conversions;

HANDLE stopEvent;
HANDLE createdThreads[1];
WinDesktopDup Duplication;
RenderTextureClass RenderTexture;
websocket_client client;
FRAME_DATA FrameData;
PTR_INFO PtrInfo;

DWORD WINAPI DuplicationThread(LPVOID);

int main(int argc, char* argv[]) {
	CoInitializeEx(nullptr,COINITBASE_MULTITHREADED);
	DWORD dwThreadId;
	DWORD dwWaitRes;
	stopEvent = CreateEvent(NULL, FALSE, FALSE, TEXT("exitEvent"));
	string error = Duplication.Initialize();
	bool Initresult = RenderTexture.Initialize(Duplication.D3DDevice, Duplication.D3DDeviceContext,
		Duplication.OutputDesc.DesktopCoordinates.right, Duplication.OutputDesc.DesktopCoordinates.bottom);
	bool TexRes = RenderTexture.InitShaders();

	string uri = "ws://192.168.8.35:9763/remote/session/devices/android/351641070150707/";
	uri.append(argv[2]);
	uri.append("?websocketToken=");
	uri.append(argv[1]);
	try {
		client.connect(to_string_t(uri)).wait();
	}
	catch (websockets::client::websocket_exception) {
		cout << "Error" << endl;
	}
	cout << "Connected" << endl;
	while (true) {
		try {
			client.receive().then([](websocket_incoming_message msg) {
				return msg.extract_string();
			}).then([&](string body) {
				cout << body << endl;
				value v1 = value::parse(to_string_t(body));
				string OpCode = to_utf8string(v1.at(U("code")).as_string());
				cout << OpCode << endl;
				if (OpCode == "REMOTE_SCREEN") {
					value v2 = value::parse(v1.at(U("payload")).as_string());
					string Operation = to_utf8string(v2.at(U("action")).as_string());
					if (Operation == "start") {
						cout << "starting Duplication thread" << endl;
						if ((error == "Success") && (Initresult) && (TexRes)) {
							createdThreads[0] = CreateThread(NULL, 0, DuplicationThread, NULL, 0, &dwThreadId);
						}
					}
					else if (Operation == "stop") {
						cout << "closing" << endl;
						SetEvent(stopEvent);
						dwWaitRes = WaitForSingleObject(createdThreads[0], INFINITE);
						switch (dwWaitRes)
						{
						case WAIT_OBJECT_0:
							cout << "Duplication thread stoped successfully";
							CloseHandle(createdThreads[0]);
						}
						//client.close().wait();
						//exit(0);
					}
				}
				if (OpCode == "REMOTE_SHELL") {
					string Command = to_utf8string(v1.at(U("payload")).as_string());
					cout << "Command is : " << Command << endl;
					websocket_outgoing_message Msg;
					value jsonStr;
					jsonStr[L"id"] = value::number((int)argv[2]);
					jsonStr[L"code"] = value::string(U("REMOTE_SHELL"));
					jsonStr[L"operationResponse"] = value::string(v1.at(U("payload")).as_string());
					jsonStr[L"status"] = value::string(U("COMPLETED"));
					string str = to_utf8string(jsonStr.serialize());
					Msg.set_utf8_message(str);
					client.send(Msg).then([]() {
						cout << "Sent" << endl;
					});
				}
			}).wait();
		}
		catch (websockets::client::websocket_exception ex) {
			cout << ex.error_code() << endl;
			exit(0);
		}
	}
	CloseHandle(stopEvent);
}

DWORD WINAPI DuplicationThread(LPVOID lpParam) {
	UNREFERENCED_PARAMETER(lpParam);
	int NameCount = 0;
	DWORD dwWaitRes;
	while (true) {
		dwWaitRes = WaitForSingleObject(stopEvent, 0);
		switch (dwWaitRes)
		{
		case WAIT_OBJECT_0:
			ExitThread(0);
		}
		if (Duplication.CaptureNext(&FrameData)) {
			Duplication.GetMouseInfo(&PtrInfo, &(FrameData.FrameInfo));
			RenderTexture.ClearRenderTarget(0.0f, 0.0f, 0.0f, 0.0f);
			RenderTexture.DrawFrame(FrameData.Frame);
			RenderTexture.DrawMouse(&PtrInfo, FrameData.Frame);
			ID3D11ShaderResourceView* sr = RenderTexture.GetShaderResourceView();
			ID3D11Resource* procRes;
			sr->GetResource(&procRes);
			ID3D11Texture2D* tex;
			procRes->QueryInterface(__uuidof(tex), (void**)&tex);
			NameCount += 1;
			wchar_t name[16];
			wsprintfW(name, L"%d", NameCount);
			wcscat_s(name, L".jpeg");
			//Duplication.SaveTextureToBmp(name,tex);
			Duplication.ConvertAndTransmit(name, tex, client);
		}
	}
}*/

#include "WebSocketSessionHandler.h"

int main(int argc, char* argv[]) {
	CoInitializeEx(nullptr, COINITBASE_MULTITHREADED);
	WebSocketSessionHandler* WebSession;
	bool initR=WebSession->GetInstance()->InitializeSession(argv[1], atoi(argv[2]), argv[3]);
	if (!initR) {
		return -1;
	}
	while (true) {
		Sleep(5000);
	}
}