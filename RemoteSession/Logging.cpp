#include "stdafx.h"
#include "Logging.h"

Logging::Logging() {
	IsInitialized = false;
}
Logging::~Logging(){
}

void Logging::InitLogging(){
	if (!IsInitialized) {
		boost::log::register_simple_formatter_factory<boost::log::trivial::severity_level, char>("Severity");
		boost::log::add_file_log(
			boost::log::keywords::file_name = "remote_session_%Y-%m-%d_%H-%M-%S.%N.log",
			boost::log::keywords::format = "[%TimeStamp%] [%ThreadID%] [%Severity%] %Message%"
		);
		boost::log::core::get()->set_filter(
			boost::log::trivial::severity >= boost::log::trivial::info
		);
		boost::log::add_common_attributes();
		IsInitialized = true;
	}
}
