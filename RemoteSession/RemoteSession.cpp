// RemoteSession.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "RemoteSession.h"
#include "WebSocketSessionHandler.h"

using namespace std;

WebSocketSessionHandler* WebSession;

//Initialize the COM interface before the desktop duplication
void InitRemoteSession(){
	CoInitializeEx(nullptr, COINITBASE_MULTITHREADED);
	WebSession = nullptr;
}

//Connect to remote session with given endpoint, operationid and uuid
bool ConnectRemoteSession(string WebUrl, string DeviceId, int OperationId, string UUIDtoValidate){
	return WebSession->GetInstance()->InitializeSession(WebUrl, DeviceId, OperationId, UUIDtoValidate);
}

//Stop the remote session
void EndRemoteSession(){
	WebSession->GetInstance()->EndSession();
}
