#pragma once

#ifdef REMOTESESSION_EXPORTS
#define REMOTESESSION_API __declspec(dllexport)
#else
#define REMOTESESSION_API __declspec(dllimport)
#endif

#include <string>

extern "C" REMOTESESSION_API void InitRemoteSession();

extern "C" REMOTESESSION_API bool ConnectRemoteSession(std::string WebUrl, std::string DeviceID, int OperationId, std::string UUIDtoValidate);

extern "C" REMOTESESSION_API void EndRemoteSession();
