#ifndef _INPUTINJECTOPERATION_H_
#define _INPUTINJECTOPERATION_H_

#include <iostream>
#include <boost/log/trivial.hpp>
#include "InputInject.h"

class InputInjectOperation {
public:
	InputInjectOperation();
	~InputInjectOperation();
	bool ProcessInputInject(_In_ int X, _In_ int Y, _In_ int Duration, _In_ std::string Action);
	bool ToggleOnScreenKeyboard(int State);

private:
	std::string LastInjectedInput;
	InputInject* InputInjectInstance;
	bool OnScreenKeyboardRunning;
};
#endif

