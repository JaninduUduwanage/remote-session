#ifndef _WEBSOCKETSESSIONHANDLER_H_
#define _WEBSOCKETSESSIONHANDLER_H_

#include <cpprest/ws_client.h>
#include <cpprest/json.h>
#include <cpprest/producerconsumerstream.h>
#include <vector>
#include "Logging.h"
#include "DuplicationOperation.h"
#include "InputInjectOperation.h"

class WebSocketSessionHandler {
public:
	static WebSocketSessionHandler* GetInstance();
	bool InitializeSession(_In_ std::string ServerUrl, _In_ std::string DeviceId, _In_ int OperationId, _In_ std::string UUUIDToValidate);
	void HandleSessionMessage(_In_ std::string Message);
	void EndSession();
	void SendMsg(_In_ std::vector<BYTE> Message);
	void SendMsg(_In_ int Id, _In_ std::string Code, _In_ std::string OperationResponse, _In_ std::string Status);

private:
	static WebSocketSessionHandler* WebSocketInstance;
	web::websockets::client::websocket_callback_client Client;
	int OperationId;
	DuplicationOperation DuplicationOp;
	InputInjectOperation InputInjectOp;
	WebSocketSessionHandler();
	Logging DataLogger;
};
#endif
