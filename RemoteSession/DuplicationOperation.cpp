#include "stdafx.h"
#include "DuplicationOperation.h"

using namespace std;

//Default constructor for DuplicationOperation
DuplicationOperation::DuplicationOperation(){
	DuplicationStopEvent = CreateEvent(NULL, FALSE, FALSE, TEXT("DuplicationStopEvent"));
	IsInitialized = false;
}

//Default destructor for DuplictionOperation
DuplicationOperation::~DuplicationOperation(){
	CloseHandle(DuplicationStopEvent);
	if (IsInitialized) {
		RenderTexture.~RenderTextureClass();
		Duplication.~WinDesktopDup();
		IsInitialized = false;
	}
}

//Initialize the duplicationOperation by initializing the desktop duplication components
bool DuplicationOperation::Initialize(){
	if (!IsInitialized) {
		bool Result = Duplication.Initialize();
		bool Initresult = RenderTexture.Initialize(Duplication.D3DDevice, Duplication.D3DDeviceContext,
			Duplication.OutputDesc.DesktopCoordinates.right, Duplication.OutputDesc.DesktopCoordinates.bottom);
		bool TexRes = RenderTexture.InitShaders();
		if ((Result) && (Initresult) && (TexRes)) {
			IsInitialized = true;
			return true;
		}
		return false;
	}
	return true;
}

//Start the desktop duplication thread
void DuplicationOperation::StartDuplicationThread(){
	DWORD DwThreadId;
	DuplicationThread = CreateThread(NULL, 0, DuplicationThreadRunner, NULL, 0, &DwThreadId);
	if (DuplicationThread == NULL) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to start Duplication thread";
		return;
	}
	BOOST_LOG_TRIVIAL(info) << "Duplication thread started successfully";
}

//Stop the desktop duplication thread
void DuplicationOperation::StopDuplicationThread(){
	if (IsInitialized) {
		DWORD DwdWaitRes;
		DwdWaitRes = WaitForSingleObject(DuplicationThread, 0);
		if (DwdWaitRes == WAIT_OBJECT_0) {
			return;
		}
		SetEvent(DuplicationStopEvent);
		DwdWaitRes = WaitForSingleObject(DuplicationThread, INFINITE);
		switch (DwdWaitRes) {
		case WAIT_OBJECT_0:
			BOOST_LOG_TRIVIAL(info) << "Duplication thread stopped successfully";
			CloseHandle(DuplicationThread);
		}
	}
}

//Function execute by the desktop duplication thread
DWORD __stdcall DuplicationOperation::DuplicationThreadRunner(LPVOID lpParam){
	UNREFERENCED_PARAMETER(lpParam);
	int NameCount = 0;
	while (true) {
		DWORD dwWaitRes;
		dwWaitRes = WaitForSingleObject(DuplicationStopEvent, 0);
		switch (dwWaitRes)
		{
		case WAIT_OBJECT_0:
			ExitThread(0);
		}
		if (Duplication.CaptureNext(&FrameData)) {
			Duplication.GetMouseInfo(&PtrInfo, &(FrameData.FrameInfo));
			RenderTexture.ClearRenderTarget(0.0f, 0.0f, 0.0f, 0.0f);
			RenderTexture.DrawFrame(FrameData.Frame);
			if (PtrInfo.Visible) {
				RenderTexture.DrawMouse(&PtrInfo, FrameData.Frame);
			}
			ID3D11ShaderResourceView* Sr = RenderTexture.GetShaderResourceView();
			ID3D11Resource* ProcRes;
			Sr->GetResource(&ProcRes);
			ID3D11Texture2D* Tex;
			ProcRes->QueryInterface(__uuidof(Tex), (void**)&Tex);
			Duplication.ConvertAndTransmit(Tex);
			Sleep(75);
		}
	}
	return 0;
}
