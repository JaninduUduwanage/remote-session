#include "stdafx.h"
#include "InputInjectOperation.h"

using namespace std;

//Default constructor
InputInjectOperation::InputInjectOperation(){
	LastInjectedInput = "up";
	InputInjectInstance = InputInject::GetInstance();
	OnScreenKeyboardRunning = false;
}

//Default disctuctor
InputInjectOperation::~InputInjectOperation(){
}

//Process Input commands for validity
bool InputInjectOperation::ProcessInputInject(int X, int Y, int Duration, string Action){
	if (Action == "move") {
		if (LastInjectedInput == "up") {
			BOOST_LOG_TRIVIAL(info) << "Unsupported input sequence " + Action + " : " + LastInjectedInput;
			return false;
		}
		UINT RetVal = InputInjectInstance->InjectMouseInput(X, Y, 0, MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE);
		if (RetVal == 1) {
			LastInjectedInput = "move";
			BOOST_LOG_TRIVIAL(info) << "Input injected : move";
			return true;
		}
		return false;
	}
	else if (Action == "up") {
		if (LastInjectedInput == "up") {
			BOOST_LOG_TRIVIAL(info) << "Unsupported input sequence " + Action + " : " + LastInjectedInput;
			return false;
		}
		UINT RetVal = InputInjectInstance->InjectMouseInput(X, Y, 0, MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTUP | MOUSEEVENTF_MOVE);
		if (RetVal == 1) {
			LastInjectedInput = "up";
			BOOST_LOG_TRIVIAL(info) << "Input injected : up";
			return true;
		}
		return false;
	}
	else if (Action == "down") {
		if (LastInjectedInput == "down" || LastInjectedInput == "move") {
			BOOST_LOG_TRIVIAL(info) << "Unsupported input sequence " + Action + " : " + LastInjectedInput;
			return false;
		}
		UINT RetVal = InputInjectInstance->InjectMouseInput(X, Y, 0, MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_MOVE);
		if (RetVal == 1) {
			LastInjectedInput = "down";
			BOOST_LOG_TRIVIAL(info) << "Input injected : down";
			return true;
		}
	}
	else {
		return false;
	}
}

//Toggle windows onscreen keyboard using default keyboard shortcut
bool InputInjectOperation::ToggleOnScreenKeyboard(int State){
	if ((State == 1)&(OnScreenKeyboardRunning)) {
		return true;
	}
	else if ((State == 0)&(!OnScreenKeyboardRunning)) {
		return true;
	}
	if (InputInjectInstance->InjectKeyboardInput(VK_LWIN, NULL) == 1) {
		if (InputInjectInstance->InjectKeyboardInput(VK_LCONTROL, NULL) == 1) {
			if (InputInjectInstance->InjectKeyboardInput(0x4F, NULL) == 1) {
				InputInjectInstance->InjectKeyboardInput(VK_LWIN, KEYEVENTF_KEYUP);
				InputInjectInstance->InjectKeyboardInput(VK_LCONTROL, KEYEVENTF_KEYUP);
				InputInjectInstance->InjectKeyboardInput(0x4F, KEYEVENTF_KEYUP);
				if (OnScreenKeyboardRunning) {
					OnScreenKeyboardRunning = false;
				}
				else {
					OnScreenKeyboardRunning = true;
				}
				return true;
			}
			return false;
		}
		return false;
	}
	return false;
}
