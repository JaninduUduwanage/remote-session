#ifndef _WINDESKTOPDUP_H_
#define _WINDESKTOPDUP_H_

#include <iostream>
#include <stdlib.h>
#include <wincodec.h>
#include <wincodecsdk.h>
#include <wrl/client.h>
#include <WTypesbase.h>
#include <d3d11.h>
#include <dxgi1_2.h>
#include <vector>
#include <fstream>
#include <boost/log/trivial.hpp>
#pragma comment(lib,"d3d11.lib")
#pragma comment(lib, "windowscodecs.lib")
#include <iostream>
#include <windows.h>

//FRAME_DATA structure
typedef struct _FRAME_DATA{
	ID3D11Texture2D* Frame;
	DXGI_OUTDUPL_FRAME_INFO FrameInfo;
} FRAME_DATA;

// Holds info about the pointer/cursor
typedef struct _PTR_INFO{
	DXGI_OUTDUPL_POINTER_SHAPE_INFO ShapeInfo;
	POINT Position;
	bool Visible;
	UINT BufferSize;
	LARGE_INTEGER LastTimeStamp;
	_Field_size_bytes_(BufferSize) BYTE* PtrShapeBuffer=nullptr;
} PTR_INFO;

class WinDesktopDup {
public:
	int    OutputNumber = 0;
	ID3D11Device*           D3DDevice = nullptr;
	ID3D11DeviceContext*    D3DDeviceContext = nullptr;
	DXGI_OUTPUT_DESC        OutputDesc;

	WinDesktopDup();
	~WinDesktopDup();

	bool Initialize();
	void  Close();
	bool  CaptureNext(_Out_ FRAME_DATA* Data);
	void  SaveTextureToBmp(_In_ PCWSTR FileName, _In_ ID3D11Texture2D* Texture);
	bool  GetMouseInfo(_Out_ PTR_INFO* PtrInfo, _In_ DXGI_OUTDUPL_FRAME_INFO* FrameInfo);
	bool  ConvertAndTransmit(_In_ ID3D11Texture2D* Texture);

private:
	IDXGIOutputDuplication* DeskDupl = nullptr;
	bool                    HaveFrameLock = false;
};
#endif