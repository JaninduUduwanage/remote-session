#include "stdafx.h"
#include "WebSocketSessionHandler.h"
#include "WinDesktopDup.h"

using namespace Microsoft::WRL;
using namespace std;

WinDesktopDup::WinDesktopDup(){
}

WinDesktopDup::~WinDesktopDup(){
	Close();
}

bool WinDesktopDup::Initialize(){
	HRESULT hr = S_OK;

	// Driver types supported
	D3D_DRIVER_TYPE DriverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	auto NumDriverTypes = ARRAYSIZE(DriverTypes);

	// Feature levels supported
	D3D_FEATURE_LEVEL FeatureLevels[] = {
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_1 };
	auto NumFeatureLevels = ARRAYSIZE(FeatureLevels);

	D3D_FEATURE_LEVEL FeatureLevel;

	// Create device
	for (size_t i = 0; i < NumDriverTypes; i++) {
		hr = D3D11CreateDevice(nullptr, DriverTypes[i], nullptr, 0, FeatureLevels, (UINT)NumFeatureLevels,
			D3D11_SDK_VERSION, &D3DDevice, &FeatureLevel, &D3DDeviceContext);
		if (SUCCEEDED(hr)) {
			BOOST_LOG_TRIVIAL(info) << "D3D11CreateDevice Success";
			break;
		}
	}
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "D3D11CreateDevice failed";
		return false;
	}

	//Initialize the Desktop Duplicaton System
	//Get DXGI device
	IDXGIDevice* DxgiDevice = nullptr;
	hr = D3DDevice->QueryInterface(__uuidof(IDXGIDevice), (void**)&DxgiDevice);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "D3DDevice->QueryInterface failed";
		return false;
	}

	//Get DXGI Adapter
	IDXGIAdapter* DxgiAdapter = nullptr;
	hr = DxgiDevice->GetParent(__uuidof(IDXGIAdapter), (void**)&DxgiAdapter);
	DxgiDevice->Release();
	DxgiDevice = nullptr;
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "DxgiDevice->GetParent failed";
		return false;
	}

	//Get Output
	IDXGIOutput* DxgiOutput = nullptr;
	hr = DxgiAdapter->EnumOutputs(OutputNumber, &DxgiOutput);
	DxgiAdapter->Release();
	DxgiAdapter = nullptr;
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "DxgiAdapter->EnumOutputs failed";
		return false;
	}

	DxgiOutput->GetDesc(&OutputDesc);

	//QI for Output1
	IDXGIOutput1* DxgiOutput1 = nullptr;
	hr = DxgiOutput->QueryInterface(__uuidof(DxgiOutput1), (void**)&DxgiOutput1);
	DxgiOutput->Release();
	DxgiOutput = nullptr;
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "DxgiOutput->QueryInterface failed";
		return false;
	}

	//Create Desktop duplication
	hr = DxgiOutput1->DuplicateOutput(D3DDevice, &DeskDupl);
	DxgiOutput1->Release();
	DxgiOutput1 = nullptr;
	if (FAILED(hr)) {
		if (hr == DXGI_ERROR_NOT_CURRENTLY_AVAILABLE) {
			BOOST_LOG_TRIVIAL(fatal) << "Too many desktop duplication already active";
			return false;
		}
		BOOST_LOG_TRIVIAL(fatal) << "Desktop duplication failed";
		return false;
	}
	return true;
}

void WinDesktopDup::Close() {
	if (DeskDupl) {
		DeskDupl->Release();
		DeskDupl = nullptr;
	}
	if (D3DDeviceContext) {
		D3DDeviceContext->Release();
		D3DDeviceContext = nullptr;
	}
	if (D3DDevice) {
		D3DDevice->Release();
		D3DDevice = nullptr;
	}
	HaveFrameLock = false;
}

bool WinDesktopDup::CaptureNext(_Out_ FRAME_DATA* Data) {
	if (!DeskDupl) {
		BOOST_LOG_TRIVIAL(fatal) << "No DXGIOutputDuplication Instance found";
		return false;
	}
	HRESULT hr;
	if (HaveFrameLock) {
		HaveFrameLock = false;
		hr = DeskDupl->ReleaseFrame();
	}
	IDXGIResource* DeskRes = nullptr;
	DXGI_OUTDUPL_FRAME_INFO FrameInfo;
	hr = DeskDupl->AcquireNextFrame(500, &FrameInfo, &DeskRes);
	if (hr == DXGI_ERROR_WAIT_TIMEOUT) {
		BOOST_LOG_TRIVIAL(fatal) << "Timeout occured while waiting till next frame";
		return false;
	}
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Acquire next frame failed";
		return false;
	}
	HaveFrameLock = true;
	ID3D11Texture2D* GpuTex = nullptr;
	hr = DeskRes->QueryInterface(__uuidof(ID3D11Texture2D), (void**)&GpuTex);
	DeskRes->Release();
	DeskRes = nullptr;
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "QI for D3D11Texture2D failed";
		return false;
	}
	Data->Frame = GpuTex;
	Data->FrameInfo = FrameInfo;
	GpuTex->Release();
	GpuTex = nullptr;
	return true;
}

void WinDesktopDup::SaveTextureToBmp(PCWSTR FileName, ID3D11Texture2D * Texture){
	HRESULT hr;

	D3D11_TEXTURE2D_DESC desc;
	Texture->GetDesc(&desc);

	GUID wicFormatGuid;
	switch (desc.Format){
	case DXGI_FORMAT_R8G8B8A8_UNORM:
		wicFormatGuid = GUID_WICPixelFormat32bppRGBA;
		break;
	case DXGI_FORMAT_B8G8R8A8_UNORM:
		wicFormatGuid = GUID_WICPixelFormat32bppBGRA;
		break;
	default:
		BOOST_LOG_TRIVIAL(fatal)<< "Unsupported DXGI_FORMAT. Only RGBA and BGRA are supported.";
	}

	// Get the device context
	ComPtr<ID3D11Device> d3dDevice;
	Texture->GetDevice(&d3dDevice);
	static ComPtr<ID3D11DeviceContext> d3dContext;
	d3dDevice->GetImmediateContext(&d3dContext);

	// map the texture
	ComPtr<ID3D11Texture2D>  mappedTexture;
	D3D11_MAPPED_SUBRESOURCE mapInfo;
	mapInfo.RowPitch;
	hr = d3dContext->Map(
		Texture,
		0, // Subresource
		D3D11_MAP_READ,
		0, // MapFlags
		&mapInfo);

	if (FAILED(hr)) {
		// If we failed to map the texture, copy it to a staging resource
		if (hr == E_INVALIDARG) {
			D3D11_TEXTURE2D_DESC desc2;
			desc2.Width = desc.Width;
			desc2.Height = desc.Height;
			desc2.MipLevels = desc.MipLevels;
			desc2.ArraySize = desc.ArraySize;
			desc2.Format = desc.Format;
			desc2.SampleDesc = desc.SampleDesc;
			desc2.Usage = D3D11_USAGE_STAGING;
			desc2.BindFlags = 0;
			desc2.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			desc2.MiscFlags = 0;

			ComPtr<ID3D11Texture2D> stagingTexture;
			hr = d3dDevice->CreateTexture2D(&desc2, nullptr, &stagingTexture);
			if (FAILED(hr)) {
				BOOST_LOG_TRIVIAL(fatal) << "Failed to create staging texture";
				//throw MyException::Make(hr, L"Failed to create staging texture");
			}

			// copy the texture to a staging resource
			d3dContext->CopyResource(stagingTexture.Get(), Texture);

			// now, map the staging resource
			hr = d3dContext->Map(
				stagingTexture.Get(),
				0,
				D3D11_MAP_READ,
				0,
				&mapInfo);
			if (FAILED(hr)) {
				BOOST_LOG_TRIVIAL(fatal) << "Failed to map staging texture";
			}

			mappedTexture = move(stagingTexture);
		}
		else {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to map texture";
		}
	}
	else {
		mappedTexture = Texture;
	}
	//auto unmapResource = Finally([&] {
	d3dContext->Unmap(mappedTexture.Get(), 0);

	ComPtr<IWICImagingFactory> wicFactory;
	hr = CoCreateInstance(
		CLSID_WICImagingFactory2,
		nullptr,
		CLSCTX_INPROC_SERVER,
		__uuidof(wicFactory),
		reinterpret_cast<void**>(wicFactory.GetAddressOf()));
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create instance of WICImagingFactory";
	}

	ComPtr<IWICBitmapEncoder> wicEncoder;
	hr = wicFactory->CreateEncoder(
		GUID_ContainerFormatPng,
		nullptr,
		&wicEncoder);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create PNG encoder";
	}

	ComPtr<IWICStream> wicStream;
	hr = wicFactory->CreateStream(&wicStream);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create IWICStream";
	}

	hr = wicStream->InitializeFromFilename(FileName, GENERIC_WRITE);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize stream from file name";
	}

	hr = wicEncoder->Initialize(wicStream.Get(), WICBitmapEncoderNoCache);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize bitmap encoder";
	}

	// Encode and commit the frame
	{
		ComPtr<IWICBitmapFrameEncode> frameEncode;
		wicEncoder->CreateNewFrame(&frameEncode, nullptr);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to create IWICBitmapFrameEncode";
		}

		hr = frameEncode->Initialize(nullptr);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize IWICBitmapFrameEncode";
		}

		hr = frameEncode->SetPixelFormat(&wicFormatGuid);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to set the pixel format";
		}

		hr = frameEncode->SetSize(desc.Width, desc.Height);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to set the output image dimensions";
		}

		hr = frameEncode->WritePixels(
			desc.Height,
			mapInfo.RowPitch,
			desc.Height * mapInfo.RowPitch,
			reinterpret_cast<BYTE*>(mapInfo.pData));
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "frameEncode->WritePixels(...) failed";
		}

		hr = frameEncode->Commit();
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to commit frameEncode";
		}
	}

	hr = wicEncoder->Commit();
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to commit encoder";
	}
}

bool WinDesktopDup::GetMouseInfo(PTR_INFO * PtrInfo, DXGI_OUTDUPL_FRAME_INFO * FrameInfo){
	if (FrameInfo->LastMouseUpdateTime.QuadPart == 0) {
		return true;
	}
	bool UpdatePosition = true;

	if (FrameInfo->PointerPosition.Visible && PtrInfo->Visible && 
		(PtrInfo->LastTimeStamp.QuadPart > FrameInfo->LastMouseUpdateTime.QuadPart)) {
		UpdatePosition = false;
	}
	if (UpdatePosition) {
		PtrInfo->Position.x = FrameInfo->PointerPosition.Position.x;
		PtrInfo->Position.y = FrameInfo->PointerPosition.Position.y;
		PtrInfo->LastTimeStamp = FrameInfo->LastMouseUpdateTime;
		PtrInfo->Visible = FrameInfo->PointerPosition.Visible != 0;
	}
	// No new shape
	if (FrameInfo->PointerShapeBufferSize == 0){
		return true;
	}
	PtrInfo->BufferSize = 0;
	// Old buffer too small
	if (FrameInfo->PointerShapeBufferSize > PtrInfo->BufferSize){
		if (PtrInfo->PtrShapeBuffer){
			delete[] PtrInfo->PtrShapeBuffer;
			PtrInfo->PtrShapeBuffer = nullptr;
		}
		PtrInfo->PtrShapeBuffer = new (std::nothrow) BYTE[FrameInfo->PointerShapeBufferSize];
		if (!PtrInfo->PtrShapeBuffer){
			BOOST_LOG_TRIVIAL(info) << "No buffer created";
			PtrInfo->BufferSize = 0;
			BOOST_LOG_TRIVIAL(fatal) << "Failed to allocate memory for pointer shape : OutOfMemory";
			return false;
		}

		// Update buffer size
		PtrInfo->BufferSize = FrameInfo->PointerShapeBufferSize;
	}

	// Get shape
	UINT BufferSizeRequired;
	HRESULT hr = DeskDupl->GetFramePointerShape(FrameInfo->PointerShapeBufferSize,
		reinterpret_cast<VOID*>(PtrInfo->PtrShapeBuffer), &BufferSizeRequired, &(PtrInfo->ShapeInfo));
	if (FAILED(hr)){
		delete[] PtrInfo->PtrShapeBuffer;
		PtrInfo->PtrShapeBuffer = nullptr;
		PtrInfo->BufferSize = 0;
		BOOST_LOG_TRIVIAL(fatal) << "Failed to get frame pointer shape";
		return false;
	}
	return true;
}

bool WinDesktopDup::ConvertAndTransmit(ID3D11Texture2D* Texture){
	HRESULT hr;

	D3D11_TEXTURE2D_DESC Desc;
	Texture->GetDesc(&Desc);
	GUID WicFormatGuid;
	switch (Desc.Format) {
	case DXGI_FORMAT_R8G8B8A8_UNORM:
		WicFormatGuid = GUID_WICPixelFormat32bppRGBA;
		break;
	case DXGI_FORMAT_B8G8R8A8_UNORM:
		WicFormatGuid = GUID_WICPixelFormat32bppBGRA;
		break;
	default:
		BOOST_LOG_TRIVIAL(fatal) << "Unsupported DXGI_FORMAT. Only RGBA and BGRA are supported.";
		return false;
	}

	// Get the device context
	ComPtr<ID3D11Device> D3dDevice;
	Texture->GetDevice(&D3dDevice);
	static ComPtr<ID3D11DeviceContext> D3dContext;
	D3dDevice->GetImmediateContext(&D3dContext);

	// map the texture
	ComPtr<ID3D11Texture2D>  MappedTexture;
	D3D11_MAPPED_SUBRESOURCE MapInfo;
	MapInfo.RowPitch;
	hr = D3dContext->Map(
		Texture,
		0, // Subresource
		D3D11_MAP_READ,
		0, // MapFlags
		&MapInfo);

	if (FAILED(hr)) {
		// If we failed to map the texture, copy it to a staging resource
		if (hr == E_INVALIDARG) {
			D3D11_TEXTURE2D_DESC Desc2;
			Desc2.Width = Desc.Width;
			Desc2.Height = Desc.Height;
			Desc2.MipLevels = Desc.MipLevels;
			Desc2.ArraySize = Desc.ArraySize;
			Desc2.Format = Desc.Format;
			Desc2.SampleDesc = Desc.SampleDesc;
			Desc2.Usage = D3D11_USAGE_STAGING;
			Desc2.BindFlags = 0;
			Desc2.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			Desc2.MiscFlags = 0;

			ComPtr<ID3D11Texture2D> StagingTexture;
			hr = D3dDevice->CreateTexture2D(&Desc2, nullptr, &StagingTexture);
			if (FAILED(hr)) {
				BOOST_LOG_TRIVIAL(fatal) << "Failed to create staging texture";
				return false;
			}

			// copy the texture to a staging resource
			D3dContext->CopyResource(StagingTexture.Get(), Texture);

			// now, map the staging resource
			hr = D3dContext->Map(
				StagingTexture.Get(),
				0,
				D3D11_MAP_READ,
				0,
				&MapInfo);
			if (FAILED(hr)) {
				BOOST_LOG_TRIVIAL(fatal) << "Failed to map staging texture";
				return false;
			}

			MappedTexture = move(StagingTexture);
		}
		else {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to map texture";
			return false;
		}
	}
	else {
		MappedTexture = Texture;
	}
	D3dContext->Unmap(MappedTexture.Get(), 0);

	ComPtr<IWICImagingFactory> WicFactory;
	hr = CoCreateInstance(
		CLSID_WICImagingFactory2,
		nullptr,
		CLSCTX_INPROC_SERVER,
		__uuidof(WicFactory),
		reinterpret_cast<void**>(WicFactory.GetAddressOf()));
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create instance of WICImagingFactory";
		return false;
	}

	ComPtr<IWICFormatConverter> WicConverter;
	hr = WicFactory->CreateFormatConverter(&WicConverter);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create format converter";
		return false;
	}

	BOOL Ability = FALSE;
	hr = WicConverter->CanConvert(WicFormatGuid, GUID_WICPixelFormat24bppBGR, &Ability);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(warning) << "Unable to querry for the ability of the conversion";
		return false;
	}
	if (!Ability) {
		BOOST_LOG_TRIVIAL(fatal) << "Cannot convert the image format";
		return false;
	}

	ComPtr<IWICBitmap> WicSourceBitmap;
	hr = WicFactory->CreateBitmapFromMemory(Desc.Width, Desc.Height, GUID_WICPixelFormat32bppBGRA, MapInfo.RowPitch,
		Desc.Height*MapInfo.RowPitch, reinterpret_cast<BYTE*>(MapInfo.pData), &WicSourceBitmap);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create bitmap from memory buffer";
		return false;
	}

	ComPtr<IWICBitmapSource> WicBitmapSource;
	hr=WicSourceBitmap->QueryInterface(__uuidof(IWICBitmapSource), reinterpret_cast<void**>(WicBitmapSource.GetAddressOf()));
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to QI for IWICBitmapSource";
		return false;
	}
	hr = WicConverter->Initialize(WicBitmapSource.Get(), GUID_WICPixelFormat24bppBGR, WICBitmapDitherTypeNone, NULL,
		0.0f, WICBitmapPaletteTypeMedianCut);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize the converter";
		return false;
	}

	ComPtr<IWICBitmapScaler> WicScaler = nullptr;
	hr = WicFactory->CreateBitmapScaler(&WicScaler);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create image scaler";
		return false;
	}

	hr = WicScaler->Initialize(WicConverter.Get(), 640, 360, WICBitmapInterpolationModeFant);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize image scaler";
		return false;
	}

	ComPtr<IWICBitmapEncoder> WicEncoder;
	hr = WicFactory->CreateEncoder(
		GUID_ContainerFormatJpeg,
		nullptr,
		&WicEncoder);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create JPEG encoder";
		return false;
	}

	ComPtr<IWICStream> WicStream;
	hr = WicFactory->CreateStream(&WicStream);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to create IWICStream";
		return false;
	}
	DWORD Size = 1024 * 128;
	vector<BYTE> VecBuffer(Size,'\t');
	hr = WicStream->InitializeFromMemory(VecBuffer.data(), Size);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize stream from Memory";
		return false;
	}

	hr = WicEncoder->Initialize(WicStream.Get(), WICBitmapEncoderNoCache);
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize bitmap encoder";
		return false;
	}

	// Encode and commit the frame
	{
		WICRect Frame;
		Frame.X = 0;
		Frame.Y = 0;
		Frame.Width = 640;
		Frame.Height = 360;

		ComPtr<IWICBitmapFrameEncode> FrameEncode;
		WicEncoder->CreateNewFrame(&FrameEncode, nullptr);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to create IWICBitmapFrameEncode";
			return false;
		}

		hr = FrameEncode->Initialize(nullptr);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to initialize IWICBitmapFrameEncode";
			return false;
		}

		hr = FrameEncode->WriteSource(WicScaler.Get(),&Frame);
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "FrameEncode->WriteSource(...) failed";
			return false;
		}

		hr = FrameEncode->Commit();
		if (FAILED(hr)) {
			BOOST_LOG_TRIVIAL(fatal) << "Failed to commit FrameEncode";
			return false;
		}
	}

	hr = WicEncoder->Commit();
	if (FAILED(hr)) {
		BOOST_LOG_TRIVIAL(info) << hr;
		BOOST_LOG_TRIVIAL(fatal) << "Failed to commit encoder";
		return false;
	}

	VecBuffer.shrink_to_fit();
	int i = 1;
	while (true) {
		if ((int)(VecBuffer[VecBuffer.size() - i]) == 217) {
			VecBuffer.erase(VecBuffer.end() - i + 1, VecBuffer.end());
			break;
		}
		else {
			i += 1;
		}
	}
	WebSocketSessionHandler* WebSession = WebSocketSessionHandler::GetInstance();
	WebSession->SendMsg(VecBuffer);
	return true;
}
