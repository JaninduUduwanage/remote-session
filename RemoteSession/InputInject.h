#ifndef _INPUTINJECT_H_
#define _INPUTINJECT_H_

#include <Windows.h>

class InputInject {
public:
	static InputInject* GetInstance();
	UINT InjectMouseInput(_In_ LONG X, _In_ LONG Y, _In_ DWORD MouseData, _In_ DWORD MouseFlags);
	UINT InjectKeyboardInput(_In_ WORD KeyCode, _In_ DWORD KeyboardFlag);
	~InputInject();

private:
	static InputInject* InputInjectInstance;
	InputInject();
};
#endif
